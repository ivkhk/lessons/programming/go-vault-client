package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	vault "github.com/hashicorp/vault/api"
	"gopkg.in/yaml.v3"
)

const configPath string = "config/config.yaml"

type Config struct {
	Servers struct {
		Vault struct {
			Token  string `yaml: "token"`
			URL    string `yaml: "url"`
			Secret string `yaml: "secret"`
			Key    string `yaml: "key"`
		} `yaml: "vault"`
		HTTP struct {
			URL string `yaml: "url"`
		} `yaml: "http"`
	} `yaml: "servers"`
}

type Data struct {
	Password string `json:"password"`
}

func main() {

	file, err := os.Open(configPath)
	if err != nil {
		fmt.Printf("Cannot open file %s\n", configPath)
		os.Exit(1)
	}
	defer file.Close()

	config := &Config{}

	decoder := yaml.NewDecoder(file)

	err = decoder.Decode(config)
	if err != nil {
		fmt.Printf("%s\n", err.Error())
		os.Exit(1)
	}

	vaultConfig := vault.DefaultConfig()
	vaultConfig.Address = config.Servers.Vault.URL

	client, err := vault.NewClient(vaultConfig)

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		os.Exit(1)
	}

	client.SetToken(config.Servers.Vault.Token)

	secret, err := client.KVv2("secret").Get(context.Background(), config.Servers.Vault.Secret)

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		os.Exit(1)
	}

	value, ok := secret.Data[config.Servers.Vault.Key].(string)

	if !ok {
		fmt.Printf("Failed to retrieve value with key: %s\n", config.Servers.Vault.Key)
		os.Exit(1)
	}

	data := Data{
		Password: value,
	}

	bytes, err := json.Marshal(data)
	if err != nil {
		fmt.Printf("%s\n", err.Error())
		os.Exit(1)
	}

	request, err := http.NewRequest(http.MethodPost, config.Servers.HTTP.URL, strings.NewReader(string(bytes)))
	if err != nil {
		fmt.Printf("%s\n", err.Error())
		os.Exit(1)
	}
	request.Header.Add("Content-Type", "application/json")
	c := &http.Client{}
	response, err := c.Do(request)
	if err != nil {
		fmt.Printf("%s\n", err.Error())
		os.Exit(1)
	}

	newData, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Printf("%s\n", err.Error())
		os.Exit(1)
	}
	fmt.Printf("Received data: %s", string(newData))

}
